// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// +build !windows

// Package uname provides a windows+linux implementation of uname(2)
package uname

import "golang.org/x/sys/unix"

func uname(fn func(u *unix.Utsname) [65]byte) string {
	var u unix.Utsname
	err := unix.Uname(&u)
	if err != nil {
		return err.Error()
	}
	b := fn(&u)
	for i, v := range b {
		if v == 0 {
			return string(b[:i])
		}
	}
	return string(b[:])
}

// Kernel returns the name of the Kernel.
// This will look like `uname -s` > `Linux`
func Kernel() string {
	return uname(func(u *unix.Utsname) [65]byte {
		return u.Sysname
	})
}

// Release returns the release of the OS
// This will look like `uname -r` > `3.10.0-693.2.2.el7.x86_64`
func Release() string {
	return uname(func(u *unix.Utsname) [65]byte {
		return u.Release
	})
}

// Version returns the version of the OS
// This will look like `uname -v` > `#1 SMP Tue Sep 12 22:26:13 UTC 2017`
func Version() string {
	return uname(func(u *unix.Utsname) [65]byte {
		return u.Version
	})
}

// NodeName returns the hostname of the system
// This will look like `uname -n` > `abex.localdomain`
func NodeName() string {
	return uname(func(u *unix.Utsname) [65]byte {
		return u.Nodename
	})
}

// Machine returns the architecture of the system
// This will look like `uname -[mpi]` > `x86_64`
func Machine() string {
	return uname(func(u *unix.Utsname) [65]byte {
		return u.Machine
	})
}
