// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// Package uname provides a basic windows+linux implementation of uname(2)
package uname

import (
	"fmt"
	"unsafe"

	"golang.org/x/sys/windows"
	"golang.org/x/sys/windows/registry"
)

var (
	kernel32                = windows.NewLazySystemDLL("kernel32.dll")
	procGetNativeSystemInfo = kernel32.NewProc("GetNativeSystemInfo")
)

// Kernel returns the name of the Kernel.
// This will look like `uname -s` > `Linux`
func Kernel() string {
	key, err := registry.OpenKey(registry.LOCAL_MACHINE, `SOFTWARE\Microsoft\Windows NT\CurrentVersion`, registry.QUERY_VALUE)
	if err != nil {
		return err.Error()
	}
	name, _, err := key.GetStringValue("ProductName")
	if err != nil {
		return err.Error()
	}
	return name
}

// Release returns the release of the OS
// This will look like `uname -r` > `3.10.0-693.2.2.el7.x86_64`
func Release() string {
	key, err := registry.OpenKey(registry.LOCAL_MACHINE, `SOFTWARE\Microsoft\Windows NT\CurrentVersion`, registry.QUERY_VALUE)
	if err != nil {
		return err.Error()
	}
	release, _, err := key.GetStringValue("ReleaseID")
	if err != nil {
		return err.Error()
	}
	build, _, err := key.GetStringValue("CurrentBuild")
	if err != nil {
		return err.Error()
	}
	return fmt.Sprintf("%v.%v", release, build)
}

// Version returns the version of the OS
// This will look like `uname -v` > `#1 SMP Tue Sep 12 22:26:13 UTC 2017`
func Version() string {
	key, err := registry.OpenKey(registry.LOCAL_MACHINE, `SOFTWARE\Microsoft\Windows NT\CurrentVersion`, registry.QUERY_VALUE)
	if err != nil {
		return err.Error()
	}
	BuildLabEx, _, err := key.GetStringValue("BuildLabEx")
	if err != nil {
		return err.Error()
	}
	return BuildLabEx
}

// NodeName returns the hostname of the system
// This will look like `uname -n` > `abex.localdomain`
func NodeName() string {
	l := uint32(64)
	u16 := make([]uint16, l)
	for {
		err := windows.GetComputerNameEx(4, &u16[0], &l)
		if err != nil {
			return err.Error()
		}
		if int(l) <= len(u16) {
			return windows.UTF16ToString(u16[:l])
		}
		l *= 2
		u16 = make([]uint16, l)
	}
}

type systemInfo struct {
	ProcessorArchitecture     uint16
	OemIDLow                  uint16
	PageSize                  uint32
	MinimumApplicationAddress uintptr
	MaximumApplicationAddress uintptr
	ActiveProcessorMask       uintptr
	NumberOfProcessors        uint32
	ProcessorType             uint32
	AlloctationGranularity    uint32
	ProcessorLevel            uint16
	ProcessorRevision         uint16
}

// Machine returns the architecture of the system
// This will look like `uname -[mpi]` > `x86_64`
func Machine() string {
	var info systemInfo
	procGetNativeSystemInfo.Call(uintptr(unsafe.Pointer(&info)))
	switch info.ProcessorArchitecture {
	case 9:
		return "x64"
	case 5:
		return "ARM"
	case 6:
		return "IA-64"
	case 0:
		return "x86"
	default:
		return "Unknown"
	}
}
