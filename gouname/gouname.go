// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package main

import (
	"fmt"

	"gitlab.com/yumeko/uname"
)

func main() {
	fmt.Println("Kernel   ", uname.Kernel())
	fmt.Println("Release  ", uname.Release())
	fmt.Println("Version  ", uname.Version())
	fmt.Println("NodeName ", uname.NodeName())
	fmt.Println("Machine  ", uname.Machine())
}
